#Muhammad Glend Angga Dwitama
#1806191396
#Tugas Pemrograman 3

import tkinter as tk
from tkinter import *
import tkinter.messagebox
import string
import sys

class Barcode(tk.Frame):
    def __init__(self, window):
        #make window
        super().__init__(window)
        self.window = window
        self.window.title("EAN-13 [by M. Glend Angga D.]")
        self.window.geometry('500x450')
        #make label
        self.label_1= tk.Label(window, text='Save barcode to PS file [eg: EAN13.eps]:')
        self.label_1.pack()
        #the entry of label
        self.SaveBarcode = StringVar()
        self.barcodeEntry = tk.Entry(window, textvariable=self.SaveBarcode)
        self.barcodeEntry.pack()
        
        #make label
        self.label_2= Label(window, text='Enter code (first 12 decimal digits):')
        self.label_2.pack()
        #make entry of label
        self.EnterCode = StringVar()
        self.EnterCodeEntry = tk.Entry(window, textvariable=self.EnterCode)
        self.EnterCodeEntry.pack()
        #make the canvas
        self.canvas = Canvas(window, bg='white')
        self.canvas.pack()
        #saved it to .eps file
        
        #continued to another function
        self.window.bind('<Return>', self.validate_input)

       
    def validate_input(self, event):
        #get the data of user's input
        new_file = self.barcodeEntry.get()
        EnterCode = self.EnterCodeEntry.get()
        
        #validate the user's input
        if not EnterCode.isdigit():
            tkinter.messagebox.showinfo('showinfo', 'Incorrect input.\nPlease enter in correct format.')
            self.EnterCodeEntry.delete(0,'end')
        elif len(EnterCode) != 12:
            tkinter.messagebox.showinfo('showinfo', 'Incorrect input.\nPlease enter in correct format.')
            self.barcodeEntry.delete(0,'end')
        elif new_file[-4:] != '.eps':
            tkinter.messagebox.showinfo('showinfo', 'Incorrect input.\nPlease enter in correct format.')
            self.barcodeEntry.delete(0,'end')
        else:
            #if all clear, then go to another function
            self.checksum()
            self.generator()

    def checksum(self):
        #get the data of user's input
        EnteredCode=self.EnterCode.get()
        
        #checksum's method
        odd=int(EnteredCode[1])+int(EnteredCode[3])+int(EnteredCode[5])+int(EnteredCode[7])+int(EnteredCode[9])+int(EnteredCode[11])
        even=int(EnteredCode[0])+int(EnteredCode[2])+int(EnteredCode[4])+int(EnteredCode[6])+int(EnteredCode[8])+int(EnteredCode[10])
        checksum = (even+(odd*3))%10
        
        if (checksum!=0):
            checkdigit = str(10-checksum)
        else:
            checkdigit = str(checksum)

        Code_13 = EnteredCode + checkdigit
        self.checkdigit = checkdigit
        self.Code_13 = Code_13
        
    def generator(self):
        #i made in into dictionary
        LCode={'0':"0001101",'1':"0011001",'2':"0010011",'3':"0111101",'4':"0100011",\
               '5':"0110001",'6':"0101111",'7':"0111011",'8':"0110111",'9':"0001011"}
        GCode={'0':"0100111",'1':"0110011",'2':"0011011",'3':"0100001",'4':"0011101",\
               '5':"0111001",'6':"0000101",'7':"0010001",'8':"0001001",'9':"0010111"}
        RCode={'0':"1110010",'1':"1100110",'2':"1101100",'3':"1000010",'4':"1011100",\
               '5':"1001110",'6':"1010000",'7':"1000100",'8':"1001000",'9':"1110100"}
        FirstGroup =    {'0':'LLLLLL',\
                         '1':'LLGLGG',\
                         '2':'LLGGLG',\
                         '3':'LLGGGL',\
                         '4':'LGLLGG',\
                         '5':'LGGLLG',\
                         '6':'LGGGLL',\
                         '7':'LGLGLG',\
                         '8':'LGLGGL',\
                         '9':'LGGLGL'}

        #to fit the bar with the user's input barcode
        fit = FirstGroup[self.Code_13[0]]

        #first line begin with 101
        start_bar= '101'
        for index in range (6):
            if fit[index] == 'L':
                str_number = self.Code_13[index+1]
                start_bar += LCode[str_number]
            else:
                str_number = self.Code_13[index+1]
                start_bar += GCode[str_number]

        #at the middle start with 01010
        mid_bar= '01010'
        for index in range (6,12):
            str_number = self.Code_13[index+1]
            mid_bar+= RCode[str_number]

        #at the end is 101
        end_bar= '101'

        #get the sum of start, mid and end of bar
        barcode = start_bar + mid_bar + end_bar
        print(barcode)

        #to renew
        self.canvas.delete ('all')

        #draw the barcode
        i=0
        for binary in barcode:
            if i in [0,2,46,48,92,94]:
                self.canvas.create_rectangle(50.5+3*i, 60, 50.5+3*(i+1), 185, fill='blue', outline='')
            elif binary == '1':
                self.canvas.create_rectangle(50.5+3*i, 60, 50.5+3*(i+1), 170, fill='black', outline='')
            else:
                self.canvas.create_rectangle(50.5+3*i, 60, 50.5+3*(i+1), 170, fill='white', outline='')
            i+=1    

       #write texts
        self.canvas.create_text(200, 40, text='EAN-13 Barcode', font=('Arial', 20))
        code = self.EnterCodeEntry.get()+self.checkdigit
        self.canvas.create_text(43.5, 200, text=code[0]+'',font=('Arial',20))
        self.canvas.create_text(120.5, 200, text=code[1:7]+'',font=('Arial',20))
        self.canvas.create_text(260.5, 200, text=code[6:], font=('Arial',20))
        self.canvas.create_text(200, 250, text='Check Digit: '+self.checkdigit,fill='green', font=('Arial', 15))
        self.canvas.postscript(file = self.barcodeEntry.get())
 

def main():
    root = tk.Tk()
    app = Barcode(root)
    app.pack(side='left')
    root.mainloop()
    
if __name__ == "__main__":
    main()
            
        

    
